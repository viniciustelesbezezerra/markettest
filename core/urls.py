from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns(
    '',
    url(r'thanks/$', views.ThanksView.as_view(), name='thanks'),
    url(r'^$', views.UserCreate.as_view(), name='add'),
)
