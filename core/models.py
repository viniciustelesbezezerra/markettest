from django.db import models
from django.core.urlresolvers import reverse


class User(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    country = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    state = models.CharField(max_length=150)

    def get_absolute_url(self):
        return reverse('core:thanks')
