from django.views.generic.edit import CreateView
from django.views.generic import TemplateView
from .models import User


class UserMixin(object):
    model = User
    fields = [
        'first_name',
        'last_name',
        'email',
        'country',
        'city',
        'state',
    ]


class UserCreate(UserMixin, CreateView):
    template_name = 'user_add.html'


class ThanksView(TemplateView):
    template_name = 'thanks.html'
